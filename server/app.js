require(`./config/config`)
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const port =  process.env.PORT
const app = express()
const rtsIndex = require(`./routes/school.routes`)
const db = require(`./controller/school.controller`)
const rtsClass = require(`./routes/class.routes`)
const rtsSec = require(`./routes/sec.routes`)
const rtsStudent = require(`./routes/student.routes`)


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
// app.all('*', function(req, res, next) {
//   var origin = req.get('origin'); 
//   res.header('Access-Control-Allow-Origin', origin);
//   res.header("Access-Control-Allow-Headers", "X-Requested-With");
//   res.header('Access-Control-Allow-Headers', 'Content-Type');
//   next();
// });
app.use(cors());

app.param([`schoolname`], (req, res, next, val) => {
  console.log(val)
  res.locals.schoolname = val
  console.log(res.locals.schoolname, ` res from `)
  next()
})

app.get('/school', db.display)

app.get('/', (request, response) => {
  console.log("entered")
  response.json(`backend is ready`)
})
app.use('/school/:schoolname', rtsIndex)
app.use('/school/:schoolname/class', rtsClass)
app.use('/school/:schoolname/sec', rtsSec)
app.use('/school/:schoolname/student',rtsStudent)
app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})