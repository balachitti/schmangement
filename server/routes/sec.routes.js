const express = require('express')
const router = express.Router()

//const std = require(`../controller/class.controller`)
const sec = require(`../controller/sec.controller`)

router.post(`/`,sec.create)
router.get(`/`,sec.display)
router.put(`/:secid`,sec.modify)



module.exports = router