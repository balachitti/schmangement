const express = require('express')
const router = express.Router()
//const student = require(`../controller/student.controller`)
const school = require(`../controller/school.controller`)

router.post(`/`,school.create)
router.delete(`/`,school.drop)

module.exports = router