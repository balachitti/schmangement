const express = require('express')
const router = express.Router()

const std = require(`../controller/class.controller`)

router.post(`/`,std.create)
router.get(`/`,std.display)
router.put(`/:classid`,std.modify)

module.exports = router