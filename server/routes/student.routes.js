const express = require('express')
const router = express.Router()

const student = require(`../controller/student.contoller`)

router.post(`/`,student.create)
router.get(`/`,student.displayAllstudents)
router.get(`/data`,student.displayAllstudentsdata)
router.get(`/details`,student.displayStudentdata)
router.get(`/details/:sid`,student.displayStudent)
router.put(`/:sid`,student.modify)
router.delete(`/:sid`,student.drop)

module.exports = router