client = require(`../helper/dbconnect`)
create = (request, response) => {
    const name = response.locals.schoolname
         //const student_id = request.body.id
     const student_name = request.body.student_name
     const seqname = name + `_autoid`
     const DOB = request.body.DOB
     const fathername = request.body.fathername
     const location = request.body.location
     const languages = request.body.languages
     const std = request.body.std
     const sec = request.body.sec
     console.log(DOB);
    const insertstudent =  `INSERT INTO ${name}.studentdetails VALUES (auto_id(),'${student_name}', '${DOB}', '${fathername}','${location}','${languages}'); `
    const insertdata = `INSERT INTO ${name}.student values 
    ((select s_id from ${name}.studentdetails where student_name='${student_name}'),(select class_id from ${name}.class where std ='${std}'),(select sec_id from ${name}.section where sec = '${sec}'))`
    client.query(`create or replace function auto_id() returns varchar as $$
    select '${name}'||nextval('${seqname}')
    $$ language sql`,(error,results)=>{
      if (error) {
        console.error(error)
            response.status(500).json({message:"error in creating function",error:error})
        console.error(error)
        //throw error
      }
      //response.status(200).json(`table created sucessfully`)
      console.log(`function created sucessfully`)
    })
    
    client.query(insertstudent,(error,results) => {
      if (error) {
        //response.status(500).json({message:"error in creating student records",error:error})
        console.error(error)
        //throw error
      }
      //response.status(200).json(`sucessfully inserted`)
      console.log(`sucessfully inserted `)
    })
    client.query(insertdata,(error,results) => {
        if (error) {
         //response.status(500).json({message:"error in creating student records",error:error})
          console.error(error)
          //throw error
        }
        //response.status(200).json(`sucessfully inserted`)
        console.log(`sucessfully inserted data1`)
      })
    response.status(200).json(`sucessfully inserted `)
 
}

modify = (request, response) => {
    const name = response.locals.schoolname
    const id = request.params.sid
    console.log(request.params.sid)
         //const student_id = request.body.id
     const student_name = request.body.student_name
     const DOB = request.body.DOB
     const fathername = request.body.fathername
     const location = request.body.location
     const languages = request.body.languages
    
     console.log(student_name);
     const updatestudentdata =`UPDATE ${name}.studentdetails SET student_name ='${student_name}',DOB ='${DOB}',fathername = '${fathername}',location ='${location}',languages = '${languages}'  WHERE s_id = '${id}';`
     console.log(updatestudentdata)
     client.query(updatestudentdata,(error,results) => {
       if (error) {
        console.error(error)
         response.status(500).json({message:"error in updating data",error:error})
        
         //throw error
       }
        response.status(200).json("updated sucessfully")
     })
}
   drop = (request,response) =>{
    console.log("delete") 
    const name = response.locals.schoolname
    const id = request.params.sid
    const deletesqlstudentdata =`DELETE FROM ${name}.studentdetails WHERE s_id ='${id}';`
     client.query(deletesqlstudentdata,(error,results)=>{
       console.log(deletesqlstudentdata)
       if (error) {
        response.status(500).json({message:"can't delete student data",error:error})
        console.error(error)
        //throw error
      }
  
       response.status(200).json("deleted sucessfully")
     })
   }
   displayAllstudents = (request,response) =>{
    console.log("student data id")
    const name = response.locals.schoolname
    //const id = request.params.sid
      const displaystudent = `SELECT * FROM ${name}.student;`
      client.query(displaystudent,(error,results) =>{
        if (error) {
          response.status(500).json({message:"error in displaying student data",error:error})
          console.error(error)
          //throw error
        }
        response.status(200).json(results.rows)
      })
    }
    displayStudentdata = (request,response) =>{
        console.log("student data")
         const name = response.locals.schoolname
         //console.log(name,"hi")
           const displaystudent = `SELECT * FROM ${name}.studentdetails`
           client.query(displaystudent,(error,results) =>{
             if (error) {
               response.status(500).json({message:"error in displaying data",error:error})
               console.error(error)
               //throw error
             }
             response.status(200).json(results.rows)
           })
       
       }
       displayStudent = (request,response) =>{
         console.log("student data id")
         const name = response.locals.schoolname
         const id = request.params.sid
           const displaystudent = `SELECT * FROM ${name}.studentdetails WHERE s_id = '${id}'`
           client.query(displaystudent,(error,results) =>{
             if (error) {
               response.status(500).json({message:"error in displaying student data",error:error})
               console.error(error)
               //throw error
             }
             response.status(200).json(results.rows)
           })
         }
      displayAllstudentsdata = (request,response) =>{
        console.log("all student data ")
        const name = response.locals.schoolname
        const id = request.params.sid
          const displayallstudentdata = `select student.s_id,student_name,dob,fathername,location,languages,std,sec from ${name}.studentdetails  join 
          ${name}.student on  studentdetails.s_id =student. s_id 
          join cvmad.class on student.class_id = class.class_id
          join cvmad.section on student.sec_id = section.sec_id;`
          client.query(displayallstudentdata,(error,results) =>{
            if (error) {
              response.status(500).json({message:"error in displaying student data",error:error})
              console.error(error)
              //throw error
            }
            response.status(200).json(results.rows)
          })
        }
module.exports = {
    create,
    modify,
    drop,
    displayAllstudents,
    displayStudentdata,
    displayAllstudentsdata,
    displayStudent   
}