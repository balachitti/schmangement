client = require(`../helper/dbconnect`)


create = (request, response) => {
    const name = response.locals.schoolname
    const std = request.body.std
    const classInssql =`insert into ${name}.class(std) values ('${std}');`
    client.query(classInssql,(error,result)=>{
        if (error) {
            response.status(500).json(`error in class creation`)
            throw error
          }
           response.status(200).json(`class inserted sucessfully`)
          console.log(`class inserted sucessfully`)
        })
}

display = (request,response) => {
    const name = response.locals.schoolname
    const classShowsql =`select * from ${name}.class;`
    client.query(classShowsql,(error,result)=> {
        if(error){
            response.status(500).json(`error in displaying class`)
            throw error
        }
        response.status(201).json(result.rows)
        console.log(`displaying class`)
    })

}

modify = (request,response) =>{
    const name = response.locals.schoolname
    const id = request.params.classid
    const std = request.body.std
    console.log(id)
    const classModsql = `update ${name}.class set std = '${std}' where class_id =${id};`
    client.query(classModsql,(error,result) =>{
        if(error){
            response.status(500).json(`error in modifying class data`)
            throw error
        }
        response.status(200).json(`successfully modifyed`)
    })
}

module.exports = {
    create,
    display,
    modify
}