client = require(`../helper/dbconnect`)

create = (request, response) => {
  const name = response.locals.schoolname
  const seqname = name + `_autoid`
  //console.log(name)
  //console.log(typeof(name))
  const createsqlschool = `CREATE SCHEMA IF NOT EXISTS ${name};`
  const createsqlreg = `CREATE TABLE IF NOT EXISTS ${name}.studentdetails
  (s_id text PRIMARY KEY, student_name varchar(30),DOB date,fathername varchar(20),location varchar(20), languages text);`
  const createclass =`CREATE TABLE IF NOT EXISTS ${name}.class (class_id serial PRIMARY KEY,std text);`
  const createsec = `CREATE TABLE IF NOT EXISTS ${name}.section (sec_id serial PRIMARY KEY,sec text);`
  const createdata = `CREATE TABLE IF NOT EXISTS ${name}.student
  (s_id varchar(30) PRIMARY KEY,class_id INT REFERENCES ${name}.class(class_id),sec_id INTEGER REFERENCES ${name}.section(sec_id));`
  const insertclass =`insert into ${name}.class(std) select std from public.class;`
  const insertsec =`insert into ${name}.section(sec) select sec from public.section;`
  client.query(createsqlschool, (error, results) => {
    if (error) {
      throw error
    }
    // response.status(200).json(`schema created sucessfully`)
    console.log(`schema created sucessfully`)
  })

  client.query(
    ` CREATE SEQUENCE ${seqname} 
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 10000;`,
    (error, results) => {
      if (error) {
        // response
        //   .status(500)
        //   .json({ message: 'error in creating sequence', error: error })
        console.error(error)
        //throw error
      }

      //response.status(200).json(`schema created sucessfully`)
      console.log(`sequence created sucessfully`)
    }
  )
  client.query(createsqlreg, (error, results) => {
    if (error) {
      // response
      //   .status(500)
      //   .json({ message: 'error in creating table', error: error })
      console.error(error)
      //throw error
    }
    //response.status(200).json(`table created sucessfully`)
    console.log(`table student details created sucessfully`)
  })
  client.query(createclass, (error, results) => {
    if (error) {
      // response
      //   .status(500)
      //   .json({ message: 'error in creating table', error: error })
      console.error(error)
      //throw error
    }
    //response.status(200).json(`table created sucessfully`)
    console.log(`table class created sucessfully`)
  })
  client.query(createsec, (error, results) => {
    if (error) {
      // response
      //   .status(500)
      //   .json({ message: 'error in creating table', error: error })
      console.error(error)
      //throw error
    }
    //response.status(200).json(`table created sucessfully`)
    console.log(`table section created sucessfully`)
  })
  client.query(createdata, (error, results) => {
    if (error) {
      console.error(error)
      // response.status(500).json({ message: 'error in creating table', error: error })
      
      //throw error
    }
    //response.status(200).json(`table created sucessfully`)
    console.log(`table student data created sucessfully`)
  })
  client.query(insertclass, (error, results) => {
    if (error) {
      console.error(error)
      //response.status(500).json({ message: 'error in creating table', error: error })
      
      //throw error
    }
    //response.status(200).json(`table created sucessfully`)
    console.log(`table class inserted sucessfully`)
  })
  client.query(insertsec, (error, results) => {
    if (error) {
      console.error(error)
      //response.status(500).json({ message: 'error in creating table', error: error })
      
      //throw error
    }
    //response.status(200).json(`table created sucessfully`)
    console.log(`table sec inserted sucessfully`)
  })
  response.status(200).json(`schema,seq and tables created sucessfully`)
}

display = (request, response) => {
    console.log('list')
    const display = `SELECT s.nspname AS schoolname from pg_catalog.pg_namespace s 
    where nspname not in ('information_schema', 'pg_catalog','public')
   and nspname not like 'pg_toast%' and nspname not like 'pg_temp_%';`
    client.query(display, (error, results) => {
      if (error) {
        response
          .status(500)
          .json({ message: 'error in displaying list', error: error })
        console.error(error)
        //throw error
      }
      response.status(200).json(results.rows)
    })
  }
  drop=(request,response)=> {
    console.log('drop school')
    const name = response.locals.schoolname
    const seqname = name + `_autoid`
    const drop = `DROP SCHEMA ${name} CASCADE`
    client.query(drop, (error, results) => {
      console.log('deleting ur school')
      if (error) {
        response
          .status(500)
          .json({ message: 'error in deleting school', error: error })
        console.error(error)
        //throw error
      }
      //response.status(200).json(`The ${name} school sucessfully dropped`)
    })
    client.query(`DROP SEQUENCE ${seqname}`, (error, results) => {
      console.log('deleting sequence')
      if (error) {
        response
          .status(500)
          .json({ message: 'error in deleting sequence', error: error })
        console.error(error)
        //throw error
      }
    })
    response
      .status(200)
      .json(`The ${name} school and ${seqname} sucessfully dropped`)
  }
    module.exports = {
    create,
    display,
    drop
}