client = require(`../helper/dbconnect`)

create = (request, response) => {
    const name = response.locals.schoolname
    const sec = request.body.sec
    const secInssql =`insert into ${name}.section(sec) values ('${sec}');`
    client.query(secInssql,(error,result)=>{
        if (error) {
            response.status(500).json(`error in sec creation`)
            throw error
          }
           response.status(200).json(`section inserted sucessfully`)
          console.log(`sec inserted sucessfully`)
        })
}

display = (request,response) => {
    const name = response.locals.schoolname
    const secShowsql =`select * from ${name}.section;`
    client.query(secShowsql,(error,result)=> {
        if(error){
            response.status(500).json(`error in displaying sec`)
            throw error
        }
        response.status(201).json(result.rows)
        console.log(`displaying sec`)
    })

}

modify = (request,response) =>{
    const name = response.locals.schoolname
    const id = request.params.secid
    const sec = request.body.sec
    console.log(id)
    const secModsql = `update ${name}.section set sec = '${sec}' where sec_id =${id};`
    client.query(secModsql,(error,result) =>{
        if(error){
            response.status(500).json(`error in modifying sec data`)
            throw error
        }
        response.status(200).json(`successfully modifyed`)
        console.log(`successfully modifyed`)
    })


}

module.exports = {
    create,
    display,
    modify
}